import React from "react";
import { connect } from "react-redux";

import { addItem } from "../../redux/cart/cart.actions";
import { checkItem, toogleItem } from "../../redux/favorite/favorite.actions";
import FavoriteButton from "../favorite-button/favorite-button.component";

import { createStructuredSelector } from "reselect";
import { selectFavoriteItem } from '../../redux/favorite/favorite.selector';

import {
  CollectionItemContainer,
  CollectionFooterContainer,
  AddButton,
  BackgroundImage,
  NameContainer,
  PriceContainer,
  ButtonWrapper
} from "./collection-item.styles";


class CollectionItem extends React.Component {
  state = {
    check: false
  }
  handleClick = state => {
    const { item, addToFavorite } = this.props;
    addToFavorite(item);
    this.setState({
      check: state
    });
  }
  componentDidMount(){
    this.checkItem();
  }
  checkItem = () => {
    const { favoriteItem, item } = this.props;
    const check = favoriteItem.some(favItem => favItem.id === item.id);
    this.setState({ check });
  }
  render() {
    const { item, addItem } = this.props;
    const { name, price, imageUrl } = item;
    return (
      <CollectionItemContainer>
        <ButtonWrapper>
          <FavoriteButton handleClick={this.handleClick} checked={this.state.check} />
        </ButtonWrapper>
        <BackgroundImage className="image" imageUrl={imageUrl} />
        <CollectionFooterContainer>
          <NameContainer>{name}</NameContainer>
          <PriceContainer>{price}</PriceContainer>
        </CollectionFooterContainer>
        <AddButton onClick={() => addItem(item)} inverted>
          Add to cart
        </AddButton>
      </CollectionItemContainer>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addItem: item => dispatch(addItem(item)),
  addToFavorite: item => dispatch(toogleItem(item)),
  checkFavoriteItem: id => dispatch(checkItem(id))
});
const mapStateToProps = createStructuredSelector({
  favoriteItem: selectFavoriteItem
})
export default connect(mapStateToProps, mapDispatchToProps)(CollectionItem);
