import styled from 'styled-components';
import CustomButton from '../custom-button/custom-button.component';

export const CollectionItemContainer = styled.div`
  width: 22vw;
  display: flex;
  flex-direction: column;
  height: 350px;
  align-items: center;
  position: relative;
  z-index: 0;
  &:hover {
    .image {
      opacity: 0.8;
    }

    button {
      opacity: 0.85;
      display: flex;
    }
  }
  @media screen and (max-width: 800px){
    width: 40vw;

    &:hover {
      .image {
        opacity: unset;
      }
  
      button {
        opacity: unset;
      }
    }
  }
`;

export const AddButton = styled(CustomButton)`
  width: 80%;
  opacity: 0.7;
  position: absolute;
  top: 255px;
  display: none;
  border: none;
  border-radius: 5px;
  font-family: 'Open Sans Condensed', sans-serif;
  transition: all .2s;

  @media screen and (max-width: 800px){
    display: block;
    opacity: 0.9;
    min-width: unset;
    padding: 0 10px;
  }
`;
export const ButtonWrapper = styled.div`
  position: absolute;
  z-index: 10;
  right: 0;
  margin-top: 10px;
`;
export const BackgroundImage = styled.div`
  width: 100%;
  height: 95%;
  background-size: cover;
  background-position: center;
  border-radius: 3px;
  margin-bottom: 5px;
  transition: all .2s;
  background-image: ${({ imageUrl }) => `url(${imageUrl})`};
`;

export const CollectionFooterContainer = styled.div`
  width: 100%;
  height: 5%;
  display: flex;
  justify-content: space-between;
  font-size: 18px;
`;

export const NameContainer = styled.span`
  width: 90%;
  margin-bottom: 15px;
`;

export const PriceContainer = styled.span`
  width: 10%;
  text-align: right;
`;
