import React from "react";
import { Checkbox } from "@material-ui/core";
import { FavoriteBorder, Favorite } from "@material-ui/icons";

import { FormControllContainer } from './favorite-button.styles';


class FavoriteButton extends React.PureComponent  {
  handleClick = (e) => {
    this.props.handleClick(e.target.checked);
  }
  render(){
    const { checked } = this.props;
    return (
      <FormControllContainer
        control={
          <Checkbox
            icon={<FavoriteBorder />}
            checkedIcon={<Favorite />}
            checked={checked}
            onClick={this.handleClick}
          />
        }
      />
    );
  }
};


export default FavoriteButton;