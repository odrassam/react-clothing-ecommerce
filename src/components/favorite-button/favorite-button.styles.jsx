import styled from 'styled-components';
import { FormControlLabel } from "@material-ui/core";

export const FormControllContainer = styled(FormControlLabel)`
  display: flex;
  justify-content: center;
  background-color: white;
  height: 40px;
  width: 40px;
  border-radius: 100px;
  box-shadow: 0 5px 5px 5px rgba(0,0,0,.1);
`;