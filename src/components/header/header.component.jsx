import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { FavoriteBorder } from "@material-ui/icons";

import CartIcon from "../cart-icon/cart-icon.component";
import CartDropdown from "../cart-dropdown/cart-dropdown.component";
import { Badge } from "@material-ui/core";
import { selectCartHidden } from "../../redux/cart/cart.selectors";
import { selectCurrentUser } from "../../redux/user/user.selectors";
import { selectTotalFavorite } from '../../redux/favorite/favorite.selector';
import { signOutStart } from "../../redux/user/user.actions";

import { ReactComponent as Logo } from "../../assets/diamond.svg";

import {
  HeaderContainer,
  LogoContainer,
  OptionsContainer,
  OptionLink
} from "./header.styles";

const Header = ({ currentUser, hidden, signOutStart, totalFavorite }) => (
  <HeaderContainer>
    <LogoContainer to="/">
      <Logo className="logo" />
    </LogoContainer>
    <OptionsContainer>
      <OptionLink to="/shop">SHOP</OptionLink>
      <OptionLink to="/favorite">
        <Badge color="secondary" badgeContent={totalFavorite}>
          <FavoriteBorder style={{ color: 'grey'}}/>
        </Badge>
      </OptionLink>
      {currentUser ? (
        <OptionLink as="div" onClick={() => signOutStart()}>
          SIGN OUT
        </OptionLink>
      ) : (
        <OptionLink to="/signin">SIGN IN</OptionLink>
      )}
      <CartIcon />
    </OptionsContainer>
    {hidden ? null : <CartDropdown />}
  </HeaderContainer>
);

const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser,
  hidden: selectCartHidden,
  totalFavorite: selectTotalFavorite
});
const mapDispatchToProps = dispatch => ({
  signOutStart: () => dispatch(signOutStart())
});
export default connect(mapStateToProps, mapDispatchToProps)(Header);
