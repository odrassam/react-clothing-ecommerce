import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const HeaderContainer = styled.div`
  height: 70px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-content: center;
  margin-bottom: 25px;

  @media screen and (max-width: 800px){
    height: 60px;
    padding: 5px;
    margin-bottom: 20px;
  }
`;

export const LogoContainer = styled(Link)`
  height: 100%;
  width: 70px;
  
`;

export const OptionsContainer = styled.div`
  width: 50%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-end;

  @media screen and (max-width: 800px){
    width: 80%;
  }
`;

export const OptionLink = styled(Link)`
  padding: 10px 15px;
  cursor: pointer;
  transition: all .2s;
  background-color: white;

  &:hover{
    box-shadow: 0 4px 5px 3px rgba(0,0,0,.2);
    transform: rotate(-4deg);
    border-radius: 4px;
  }
`;
