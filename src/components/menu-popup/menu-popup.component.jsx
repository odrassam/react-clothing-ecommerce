import React from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

const MenuTitle = ["Sneakers", "Jackets", "Hats"];
export default function SimpleMenu() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [index, setIndex] = React.useState(0);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (e, id) => {
    setIndex(id);
    console.log(id)
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        {index === 'backdropClick' ? 'Category' : MenuTitle[index]}
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {MenuTitle.map((item,index) => {
          return <MenuItem onClick={event => handleClose(event, index)}>{item}</MenuItem>;
        })}
      </Menu>
    </div>
  );
}
