import React from "react";
import { connect } from "react-redux";

import CollectionItem from "../../components/collection-item/collection-item.component";

import { selectFavoriteItem } from "../../redux/favorite/favorite.selector";
import { createStructuredSelector } from "reselect";

import {
  CollectionPageContainer,
  CollectionItemsContainer,
  CollectionTitle,
  CollectionNoData
} from "./favorite.styles";

const CollectionPage = ({ collection }) => {
  const renderItem = () => {
    if (collection.length !== 0) {
      return (
        <CollectionItemsContainer>
          {
            collection.map(item => (
              <CollectionItem key={item.id} item={item} />
              ))
          }
        </CollectionItemsContainer>
      );
    }
    return <CollectionNoData>No Whistlist Founded</CollectionNoData>;
  };
  return (
    <CollectionPageContainer>
      <CollectionTitle>Favorites</CollectionTitle>
      {renderItem()}
    </CollectionPageContainer>
  );
};

const mapStateToProps = createStructuredSelector({
  collection: selectFavoriteItem
});

export default connect(mapStateToProps)(CollectionPage);
