import React from "react";

import SignIn from "../../components/sign-in/sign-in.component";
import SignUp from "../../components/sign-up/sign-up.component";

import {
  SignInAndSignUpContainer,
  SignInSignUpIcon,
} from "./sign-in-and-sign-up.styles";

class SignInAndSignUpPage extends React.Component {
  state = {
    toogle: false,
  };
  switchPanel = () => {
    this.setState({
      toogle: !this.state.toogle,
    });
  };
  showPanel = () => {
    const { toogle } = this.state;
    if (!toogle) {
      return <SignIn switchPanel={this.switchPanel} />;
    }
    return <SignUp switchPanel={this.switchPanel} />;
  };
  render() {
    return (
      <SignInAndSignUpContainer>
        <feature>
          <SignInSignUpIcon />
        </feature>
        {this.showPanel()}
      </SignInAndSignUpContainer>
    );
  }
}

export default SignInAndSignUpPage;
