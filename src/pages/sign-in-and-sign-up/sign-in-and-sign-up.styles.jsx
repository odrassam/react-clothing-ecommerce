import styled from "styled-components";
import { ReactComponent as LoginDoorSVG } from "../../assets/login.svg";

export const SignInSignUpIcon = styled(LoginDoorSVG)`
  width: 60%;
  height: 0%;
  @media only screen and (max-width: 50em) {
    display: none;
    width: 0;
    height: 0;
  }
`;
export const SignInAndSignUpContainer = styled.div`
  width: 80vw;
  margin: 30px auto;
  padding-top: 30px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  align-content: center;
  align-items: start;
  grid-column-gap: 30px;

  @media only screen and (max-width: 50em) {
    grid-template-columns: 1fr;
    justify-items: center;
    align-items: center;
  }
`;
